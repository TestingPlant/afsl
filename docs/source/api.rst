API Reference
=============

.. doxygennamespace:: afsl
   :members:

Exceptions
==========

All functions may throw exceptions. Since this is a wrapper around the filesystem library, functions may throw
std::filesystem::filesystem_error in addition to afsl::error.
