afsl
====
afsl (**a**\ nother **f**\ ile\ **s**\ ystem **l**\ ibrary) is a library which provides file manipulation utilities.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   examples
   api

.. image:: https://repology.org/badge/vertical-allrepos/afsl.svg?header=afsl
   :target: https://repology.org/project/afsl/versions
