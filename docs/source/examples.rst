Examples
========

Reading from a file:

.. code-block:: c++

   const std::string file_contents = afsl::read_from_file("/path/to/file.txt", afsl::file_mode::text);

Writing to a file:

.. code-block:: c++

   afsl::write_to_file("/path/to/file.txt", "file data", afsl::file_mode::text);

Creating a temporary directory:

.. code-block:: c++

   const std::filesystem::path directory = afsl::create_temporary_directory("program-name-");

.. code-block:: c++

   const afsl::file directory(afsl::create_temporary_directory());
   // directory will be removed after it goes out of scope

Creating a temporary file:

.. code-block:: c++

   const std::filesystem::path file = afsl::create_temporary_file("file contents", afsl::file_mode::text, "program-name-", ".txt");

.. code-block:: c++

   const afsl::file file(afsl::create_temporary_file("file contents", afsl::file_mode::text));
   // file will be removed after it goes out of scope
