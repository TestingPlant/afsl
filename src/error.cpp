#include "afsl.hpp"
#include <filesystem>
#include <system_error>

namespace afsl {
error::error(std::error_code error_code, std::filesystem::path path)
    : system_error(error_code, path.string()), path(path) {}

error::error(int error_number, std::filesystem::path path)
    : system_error(error_number, std::generic_category(), path.string()), path(path) {}

const std::filesystem::path &error::get_path() const { return path; }
} // namespace afsl
