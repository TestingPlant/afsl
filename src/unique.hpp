#pragma once

#ifndef AFSL_HPP
#error "Only include <afsl/afsl.hpp> directly"
#endif

#include "file.hpp"
#include <filesystem>
#include <string_view>

namespace afsl {
/*!
 * Creates a directory with a random name.
 *
 * \param parent Directory to create the directory in.
 * \param prefix Prefix of the directory name.
 * \param suffix Suffix of the directory name.
 */
std::filesystem::path create_unique_directory(const std::filesystem::path &parent, const std::string_view prefix = "",
                                              const std::string_view suffix = "");

/*!
 * Creates a file with a random name.
 *
 * \param parent Directory to create the file in.
 * \param data Data to write to the file.
 * \param mode Mode to open the file with.
 * \param prefix Prefix of the filename.
 * \param suffix Suffix of the filename.
 */
std::filesystem::path create_unique_file(const std::filesystem::path &parent, const std::string_view data,
                                         const file_mode mode, const std::string_view prefix = "",
                                         const std::string_view suffix = "");
} // namespace afsl
