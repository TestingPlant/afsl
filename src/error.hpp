#pragma once

#ifndef AFSL_HPP
#error "Only include <afsl/afsl.hpp> directly"
#endif

#include <filesystem>
#include <system_error>

namespace afsl {
class error : public std::system_error {
private:
	const std::filesystem::path path;

public:
	/*!
	 * \param error_code The error code.
	 * \param path Path that the error is about.
	 */
	error(std::error_code error_code, std::filesystem::path path);

	/*!
	 * \param error_number The error number.
	 * \param path Path that the error is about.
	 */
	error(int error_number, std::filesystem::path path);

	const std::filesystem::path &get_path() const;
};
} // namespace afsl
