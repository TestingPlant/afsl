#include "afsl.hpp"
#include <cerrno>
#include <cstddef>
#include <filesystem>
#include <fstream>
#include <string>
#include <string_view>
#include <utility>

namespace afsl {
file::create_directory_t file::create_directory = create_directory_t();

file::file(std::filesystem::path path, const bool remove_all) : path(std::move(path)), remove_all(remove_all) {}

file::file(std::filesystem::path path, const create_directory_t &, const bool remove_all)
    : path(std::move(path)), remove_all(remove_all) {
	try {
		if (!std::filesystem::create_directory(**this))
			throw error(std::make_error_code(std::errc::file_exists), **this);
	} catch (const std::filesystem::filesystem_error &filesystem_error) {
		throw error(filesystem_error.code(), **this);
	}
}

file::file(std::filesystem::path path, const std::string_view data, const file_mode mode)
    : path(std::move(path)), remove_all(false) {
	create_file(**this, data, mode);
}

file::~file() {
	if (remove_all)
		std::filesystem::remove_all(**this);
	else
		std::filesystem::remove(**this);
}

const std::filesystem::path &file::operator*() const { return path; }

const std::filesystem::path *file::operator->() const { return &path; }

std::string read_from_file(const std::filesystem::path &path, const file_mode mode) {
	try {
		std::ifstream file;
		file.exceptions(std::ios::badbit | std::ios::failbit);

		file.open(path, mode == file_mode::binary ? std::ios::binary : std::ios::in);

		file.seekg(0, std::ios::end);
		const std::size_t file_size = file.tellg();
		file.seekg(0, std::ios::beg);

		std::string data(file_size, '\0');
		file.read(data.data(), data.size());
		return data;
	} catch (const std::ios_base::failure &) {
		throw error(errno, path);
	}
}

void write_to_file(const std::filesystem::path &path, const std::string_view data, const file_mode mode) {
	try {
		std::ofstream file;
		file.exceptions(std::ios::badbit | std::ios::failbit);

		file.open(path, mode == file_mode::binary ? std::ios::binary | std::ios::trunc : std::ios::trunc);
		file.write(data.data(), data.size());
		file.close();
	} catch (const std::ios_base::failure &) {
		throw error(errno, path);
	}
}

void create_file(const std::filesystem::path &path, const std::string_view data, const file_mode mode) {
	bool already_exists;
	create_file(path, data, mode, already_exists);

	if (already_exists)
		throw error(std::make_error_code(std::errc::file_exists), path);
}

void create_file(const std::filesystem::path &path, const std::string_view data, const file_mode mode,
                 bool &already_exists) {
	const file temporary_directory(create_temporary_directory(), true);
	const std::filesystem::path temporary_path = *temporary_directory / "a";
	write_to_file(temporary_path, data, mode);

	std::error_code error_code;
	std::filesystem::copy(temporary_path, path, error_code);
	if (error_code) {
		if (error_code == std::errc::file_exists)
			already_exists = true;
		else
			throw error(error_code, path);
	} else {
		already_exists = false;
	}
}
} // namespace afsl
