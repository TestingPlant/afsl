#include "afsl.hpp"
#include <filesystem>
#include <fmt/format.h>
#include <random>
#include <string_view>

namespace afsl {
std::filesystem::path create_unique_directory(const std::filesystem::path &parent, const std::string_view prefix,
                                              const std::string_view suffix) {
	static thread_local std::default_random_engine random(std::random_device{}());

	while (true) {
		const std::filesystem::path directory = parent / fmt::format("{}{:x}{}", prefix, random(), suffix);
		if (std::filesystem::create_directory(directory))
			return directory;
	}
}

std::filesystem::path create_unique_file(const std::filesystem::path &parent, const std::string_view data,
                                         const file_mode mode, const std::string_view prefix,
                                         const std::string_view suffix) {
	static thread_local std::default_random_engine random(std::random_device{}());

	while (true) {
		const std::filesystem::path file = parent / fmt::format("{}{:x}{}", prefix, random(), suffix);
		bool already_exists;
		create_file(file, data, mode, already_exists);
		if (!already_exists)
			return file;
	}
}
} // namespace afsl
