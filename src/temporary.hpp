#pragma once

#ifndef AFSL_HPP
#error "Only include <afsl/afsl.hpp> directly"
#endif

#include "file.hpp"
#include <filesystem>
#include <string_view>

namespace afsl {
/*!
 * Creates a temporary directory. Its permissions will be set to std::filesystem::perms::owner_all.
 *
 * It's suggested that you wrap this with afsl::file to automatically remove the directory.
 *
 * \param prefix Prefix of the directory name.
 * \param suffix Suffix of the directory name.
 */
std::filesystem::path create_temporary_directory(const std::string_view prefix = "",
                                                 const std::string_view suffix = "");

/*!
 * Creates a temporary file. Its permissions will be set to std::filesystem::perms::owner_read |
 * std::filesystem::perms::owner_write.
 *
 * It's suggested that you wrap this with afsl::file to automatically remove the file.
 *
 * \param data Data to write to the file.
 * \param mode Mode to open the file with.
 * \param prefix Prefix of the filename.
 * \param suffix Suffix of the filename.
 */
std::filesystem::path create_temporary_file(const std::string_view data, const file_mode mode,
                                            const std::string_view prefix = "", const std::string_view suffix = "");
} // namespace afsl
