#include "afsl.hpp"
#include <filesystem>
#include <string_view>

namespace afsl {
std::filesystem::path create_temporary_directory(const std::string_view prefix, const std::string_view suffix) {
	const std::filesystem::path directory =
	    create_unique_directory(std::filesystem::temp_directory_path(), prefix, suffix);
	std::filesystem::permissions(directory, std::filesystem::perms::owner_all,
	                             std::filesystem::perm_options::replace);
	return directory;
}

std::filesystem::path create_temporary_file(const std::string_view data, const file_mode mode,
                                            const std::string_view prefix, const std::string_view suffix) {
	const std::filesystem::path file =
	    create_unique_file(std::filesystem::temp_directory_path(), data, mode, prefix, suffix);
	std::filesystem::permissions(file, std::filesystem::perms::owner_read | std::filesystem::perms::owner_write,
	                             std::filesystem::perm_options::replace);
	return file;
}
} // namespace afsl
