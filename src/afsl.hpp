#pragma once

#define AFSL_HPP

#include "error.hpp"
#include "file.hpp"
#include "temporary.hpp"
#include "unique.hpp"
