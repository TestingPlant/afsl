#pragma once

#ifndef AFSL_HPP
#error "Only include <afsl/afsl.hpp> directly"
#endif

#include <filesystem>
#include <string>
#include <string_view>

namespace afsl {
/*!
 * Mode which specifies how to open a file.
 */
enum class file_mode {
	/*!
	 * Binary mode. When reading from a file, this will give you the exact contents of that file, and when
	 * writing to a file, the data passed will be what's written to the file.
	 */
	binary,

	/*!
	 * Text mode. When reading and writing files, some systems will manipulate the contents of the file, such as
	 * whitespace.
	 */
	text
};

/*!
 * Object which removes a file when destructed.
 */
class file {
private:
	const std::filesystem::path path;
	const bool remove_all;

public:
	struct create_directory_t {};
	static create_directory_t create_directory;

	/*!
	 * Removes the file passed when the file is destructed.
	 *
	 * \param path Path to the file to remove when this object is destructed.
	 * \param remove_all If false and a directory is passed, the directory must be empty before this is
	 *                     destructed. If true, the destructor will remove non-empty directories.
	 */
	file(std::filesystem::path path, const bool remove_all = false);

	/*!
	 * Creates a directory which will be removed when destructed.
	 *
	 * \param path Path of the directory to create.
	 * \param create_directory_value Pass afsl::file::create_directory.
	 * \param remove_all If false, the directory must be empty before this is destructed. If true, the destructor
	 *                   will remove non-empty directories.
	 */
	file(std::filesystem::path path, const create_directory_t &create_directory_value,
	     const bool remove_all = false);

	/*!
	 * Creates a file which will be removed when destructed.
	 *
	 * \param path Path of the file to create.
	 * \param data Data to write to the file.
	 * \param mode Mode to open the file with.
	 */
	file(std::filesystem::path path, const std::string_view data, const file_mode mode);

	~file();

	/*!
	 * Gets the path of the file.
	 */
	const std::filesystem::path &operator*() const;

	/*!
	 * Gets the path of the file.
	 */
	const std::filesystem::path *operator->() const;
};

/*!
 * Reads data from a file.
 *
 * \param path Path of the file to read from.
 * \param mode Mode to open the file with.
 */
std::string read_from_file(const std::filesystem::path &path, const file_mode mode);

/*!
 * Writes data to a file.
 *
 * \param path Path of the file to write to.
 * \param data Data to write to the file.
 * \param mode Mode to open the file with.
 */
void write_to_file(const std::filesystem::path &path, const std::string_view data, const file_mode mode);

/*!
 * Creates a file. If the file already exists, afsl::error will be thrown with std::errc::file_exists.
 *
 * \param path Path of the file to create.
 * \param data Data to write to the file.
 * \param mode Mode to open the file with.
 */
void create_file(const std::filesystem::path &path, const std::string_view data, const file_mode mode);

/*!
 * Creates a file. If the file already exists, nothing will be done and already_exists will be set.
 *
 * \param path Path of the file to create.
 * \param data Data to write to the file.
 * \param mode Mode to open the file with.
 * \param already_exists Whether the file already existed.
 */
void create_file(const std::filesystem::path &path, const std::string_view data, const file_mode mode,
                 bool &already_exists);
} // namespace afsl
