#include "data.hpp"
#include <afsl.hpp>
#include <filesystem>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string>

TEST(UniqueTest, DirectoryCreation) {
	const std::filesystem::path temp = std::filesystem::temp_directory_path();
	const std::string prefix = "prefix-";
	const std::string suffix = "-suffix";

	const afsl::file directory(afsl::create_unique_directory(temp, prefix, suffix));
	EXPECT_EQ(directory->parent_path(), temp);
	EXPECT_THAT(directory->filename(), testing::AllOf(testing::StartsWith(prefix), testing::EndsWith(suffix)));
	EXPECT_TRUE(std::filesystem::is_directory(*directory));
	EXPECT_EQ(std::filesystem::status(*directory).permissions(), default_directory_perms);
}

TEST(UniqueTest, FileCreation) {
	const std::filesystem::path temp = std::filesystem::temp_directory_path();
	const std::string prefix = "prefix-";
	const std::string suffix = "-suffix";

	const afsl::file file(afsl::create_unique_file(temp, binary_content, afsl::file_mode::binary, prefix, suffix));
	EXPECT_EQ(file->parent_path(), temp);
	EXPECT_EQ(afsl::read_from_file(*file, afsl::file_mode::binary), binary_content);
	EXPECT_THAT(file->filename(), testing::AllOf(testing::StartsWith(prefix), testing::EndsWith(suffix)));
	EXPECT_TRUE(std::filesystem::is_regular_file(*file));
	EXPECT_EQ(std::filesystem::status(*file).permissions(), default_file_perms);
}
