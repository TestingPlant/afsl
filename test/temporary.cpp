#include "data.hpp"
#include <afsl.hpp>
#include <filesystem>
#include <gtest/gtest.h>

TEST(TemporaryTest, TemporaryDirectory) {
	const std::filesystem::path temp = std::filesystem::temp_directory_path();

	const afsl::file directory(afsl::create_temporary_directory());
	EXPECT_EQ(directory->parent_path(), temp);
	EXPECT_TRUE(std::filesystem::is_directory(*directory));
	EXPECT_EQ(std::filesystem::status(*directory).permissions(), std::filesystem::perms::owner_all);
}

TEST(TemporaryTest, TemporaryFile) {
	const std::filesystem::path temp = std::filesystem::temp_directory_path();

	const afsl::file file = afsl::create_temporary_file(binary_content, afsl::file_mode::binary);
	EXPECT_EQ(file->parent_path(), temp);
	EXPECT_EQ(afsl::read_from_file(*file, afsl::file_mode::binary), binary_content);
	EXPECT_TRUE(std::filesystem::is_regular_file(*file));
	EXPECT_EQ(std::filesystem::status(*file).permissions(),
	          std::filesystem::perms::owner_read | std::filesystem::perms::owner_write);
}
