#include "data.hpp"
#include <afsl.hpp>
#include <filesystem>
#include <gtest/gtest.h>

template <typename Callback> void expect_error(Callback callback, const afsl::error &expected_error) {
	bool threw_error = false;

	try {
		callback();
	} catch (const afsl::error &error) {
		threw_error = true;
		EXPECT_EQ(std::string(error.what()), std::string(expected_error.what()));
		EXPECT_EQ(error.code(), expected_error.code());
		EXPECT_EQ(error.get_path(), expected_error.get_path());
	}

	EXPECT_TRUE(threw_error);
}

TEST(FileTest, NoSpaceLeftError) {
	const std::filesystem::path full("/dev/full");
	if (!std::filesystem::exists(full))
		GTEST_SKIP() << "/dev/full does not exist. The NoSpaceLeftError test will be skipped.";

	for (const afsl::file_mode mode : {afsl::file_mode::binary, afsl::file_mode::text}) {
		expect_error([&]() { afsl::write_to_file(full, "a", mode); },
		             afsl::error(std::make_error_code(std::errc::no_space_on_device), full));
	}
}

TEST(FileTest, IsDirectoryError) {
	const std::filesystem::path directory = std::filesystem::temp_directory_path();

	for (const afsl::file_mode mode : {afsl::file_mode::binary, afsl::file_mode::text}) {
		expect_error([&]() { afsl::read_from_file(directory, mode); },
		             afsl::error(std::make_error_code(std::errc::invalid_argument), directory));
		expect_error([&]() { afsl::write_to_file(directory, "", mode); },
		             afsl::error(std::make_error_code(std::errc::is_a_directory), directory));
	}
}

TEST(FileTest, ReadWrite) {
	const afsl::file directory(afsl::create_temporary_directory(), true);
	const std::filesystem::path path = *directory / "a";

	afsl::write_to_file(path, binary_content, afsl::file_mode::binary);
	EXPECT_EQ(afsl::read_from_file(path, afsl::file_mode::binary), binary_content);

	afsl::write_to_file(path, text_content, afsl::file_mode::text);
	EXPECT_EQ(afsl::read_from_file(path, afsl::file_mode::text), text_content);
}

TEST(FileTest, CreateFile) {
	const afsl::file directory(afsl::create_temporary_directory(), true);
	const std::filesystem::path path = *directory / "a";
	bool already_exists;

	afsl::create_file(path, binary_content, afsl::file_mode::binary);
	EXPECT_EQ(afsl::read_from_file(path, afsl::file_mode::binary), binary_content);
	expect_error([&]() { afsl::create_file(path, "", afsl::file_mode::binary); },
	             afsl::error(std::make_error_code(std::errc::file_exists), path));
	std::filesystem::remove(path);

	afsl::create_file(path, binary_content, afsl::file_mode::binary, already_exists);
	EXPECT_FALSE(already_exists);
	EXPECT_EQ(afsl::read_from_file(path, afsl::file_mode::binary), binary_content);
	afsl::create_file(path, binary_content, afsl::file_mode::binary, already_exists);
	EXPECT_TRUE(already_exists);
	std::filesystem::remove(path);
}

TEST(FileTest, RegularFile) {
	const afsl::file directory(afsl::create_temporary_directory());
	const std::filesystem::path path = *directory / "file";

	afsl::write_to_file(path, "", afsl::file_mode::binary);

	{
		const afsl::file file(path);
		EXPECT_EQ(*file, path);
		EXPECT_TRUE(std::filesystem::exists(path));
	}
	EXPECT_FALSE(std::filesystem::exists(path));
}

TEST(FileTest, EmptyDirectory) {
	const std::filesystem::path path = afsl::create_temporary_directory();
	{
		const afsl::file file(path);
		EXPECT_EQ(*file, path);
		EXPECT_TRUE(std::filesystem::exists(path));
	}
	EXPECT_FALSE(std::filesystem::exists(path));
}

TEST(FileTest, NonemptyDirectory) {
	const std::filesystem::path path = afsl::create_temporary_directory();
	afsl::write_to_file(path / "file", "", afsl::file_mode::binary);
	{
		const afsl::file file(path, true);
		EXPECT_EQ(*file, path);
		EXPECT_TRUE(std::filesystem::exists(path));
	}
	EXPECT_FALSE(std::filesystem::exists(path));
}

TEST(FileTest, CreateDirectory) {
	const afsl::file directory(afsl::create_temporary_directory());

	const std::filesystem::path other_directory_path = *directory / "a";
	const afsl::file other_directory(other_directory_path, afsl::file::create_directory);

	EXPECT_TRUE(std::filesystem::is_directory(*other_directory));
	EXPECT_EQ(*other_directory, other_directory_path);
	EXPECT_EQ(std::filesystem::status(*other_directory).permissions(), default_directory_perms);

	expect_error([&]() { const afsl::file another_directory(other_directory_path, afsl::file::create_directory); },
	             afsl::error(std::make_error_code(std::errc::file_exists), other_directory_path));
}

TEST(FileTest, CreateRegularFile) {
	const afsl::file directory(afsl::create_temporary_directory());

	const std::filesystem::path file_path = *directory / "a";
	const afsl::file file(file_path, binary_content, afsl::file_mode::binary);
	EXPECT_TRUE(std::filesystem::is_regular_file(*file));
	EXPECT_EQ(*file, file_path);
	EXPECT_EQ(std::filesystem::status(*file).permissions(), default_file_perms);
}
