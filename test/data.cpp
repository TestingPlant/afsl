#include "data.hpp"
#include <filesystem>
#include <string>

const std::string binary_content = {'a', '\r', '\n', '\0', '\n', 'b'};
const std::string text_content = "content\n";
const std::filesystem::perms default_directory_perms =
    std::filesystem::perms::owner_all | std::filesystem::perms::group_read | std::filesystem::perms::group_exec |
    std::filesystem::perms::others_read | std::filesystem::perms::others_exec;
const std::filesystem::perms default_file_perms =
    std::filesystem::perms::owner_read | std::filesystem::perms::owner_write | std::filesystem::perms::group_read |
    std::filesystem::perms::others_read;
