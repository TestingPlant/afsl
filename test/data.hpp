#include <filesystem>
#include <string>

extern const std::string binary_content;
extern const std::string text_content;
extern const std::filesystem::perms default_directory_perms;
extern const std::filesystem::perms default_file_perms;
